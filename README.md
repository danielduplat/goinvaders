# Go Invaders

A space invader like game with a 2D engine



## Dependencies

[GitHub - jmeubank/tdm-gcc: TDM-GCC is a cleverly disguised GCC compiler for Windows!](https://github.com/jmeubank/tdm-gcc)

[GitHub - libsdl-org/SDL: Simple Directmedia Layer](https://github.com/libsdl-org/SDL)

https://go.dev/

## Build Instructions

Get the Go project dependencies

```powershell
go mod tidy
```

 Run the Powershell script if on Windows or the Shell Script if on Linux

```powershell
./build.ps1
```

```bash
./build.sh
```


