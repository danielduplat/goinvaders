module GoInvaders

go 1.19

require github.com/veandco/go-sdl2 v0.4.25

require (
	github.com/go-gl/mathgl v1.0.0
	golang.org/x/image v0.0.0-20190321063152-3fc05d484e9f // indirect
)
