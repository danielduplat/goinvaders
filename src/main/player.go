package main

import (
	"GoInvaders/src/engine"
	"fmt"

	"github.com/go-gl/mathgl/mgl64"
	"github.com/veandco/go-sdl2/sdl"
)

const (
	PLAYER_SPEED = 0.1
	PLAYER_SIZE  = 105
)

type Player struct {
	sprite   engine.Sprite
	position mgl64.Vec2
}

func newPlayer(renderer *sdl.Renderer, windowHeight, windowWidth float64) (Player, error) {
	var p Player
	var err error

	p.sprite, err = engine.NewSprite(fmt.Sprintf(ASSETS_FOLDER_PATH, "playerV1.bmp"), renderer)

	if err != nil {
		return Player{}, fmt.Errorf("\nerror creating player sprite: %v", err)
	}

	p.position = mgl64.Vec2{float64(windowWidth / 2.0), float64(windowHeight - PLAYER_SIZE/2.0)}

	return p, nil
}

func (p *Player) update(window *engine.GameWindow) {
	keys := sdl.GetKeyboardState()

	if keys[sdl.SCANCODE_LEFT] == 1 {
		destination := mgl64.Vec2{p.position.X() - PLAYER_SPEED, p.position.Y()}

		if !window.IsPositionOutOfBounds(destination, PLAYER_SIZE, PLAYER_SIZE) {
			p.position = destination
		}
	} else if keys[sdl.SCANCODE_RIGHT] == 1 {
		destination := mgl64.Vec2{p.position.X() + PLAYER_SPEED, p.position.Y()}

		if !window.IsPositionOutOfBounds(destination, PLAYER_SIZE, PLAYER_SIZE) {
			p.position = destination
		}
	}
}

func (p Player) GetRenderTarget() engine.RenderTarget {
	return engine.RenderTarget{Sprite: p.sprite, Position: p.position, Height: PLAYER_SIZE, Width: PLAYER_SIZE}
}
