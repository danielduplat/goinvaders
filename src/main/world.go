package main

import "GoInvaders/src/engine"

type world struct {
	currentMap *gameMap
	renderer   engine.GameRenderer
	window     engine.GameWindow
}
