package main

import (
	"GoInvaders/src/engine"
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

// Inicializa a biblioteca SDL
func initSDL() {
	if err := sdl.Init(uint32(sdl.INIT_EVERYTHING)); err != nil {
		panic(err)
	}
}

func main() {
	initSDL()
	window := engine.NewWindow("Space Invaders", 800, 600)

	renderer, err := engine.InitRenderer(window.Instance)
	if err != nil {
		fmt.Println("Error on Create Renderer:", err)
		return
	}

	player, err := newPlayer(renderer.Instance, window.Height, window.Width)
	if err != nil {
		fmt.Println("Error Creating New Player:", err)
		return
	}

	var enemies []Enemy
	c := 1.0 + ENEMY_SIZE/2

	for qnt := 5; qnt > 0; qnt-- {
		gameEnemy, err := newEnemy(renderer.Instance, c, window.Height/2.0)

		if err != nil {
			fmt.Println("Error Creating enemy:", err)
			return
		}

		c += ENEMY_SIZE + 2

		enemies = append(enemies, gameEnemy)
	}

	defer renderer.Instance.Destroy()
	defer window.Instance.Destroy()
	defer sdl.Quit()

	for {

		for event := sdl.PollEvent(); event != nil; event = sdl.PollEvent() {
			switch event.(type) {
			case *sdl.QuitEvent:
				return
			}
		}

		renderer.SetDrawColor(engine.COLORS.White)
		renderer.Instance.Clear()

		player.update(&window)

		renderer.Draw(player, 1.0, 0)

		for _, enemy := range enemies {
			renderer.Draw(enemy, 1.0, 180)
		}

		renderer.Instance.Present()
	}
}
