package main

import (
	"GoInvaders/src/engine"
	"fmt"

	"github.com/go-gl/mathgl/mgl64"
	"github.com/veandco/go-sdl2/sdl"
)

const ENEMY_SIZE = 105
const ENEMY_SPRITE_FILE = "enemyV1.bmp"

type Enemy struct {
	sprite   engine.Sprite
	position mgl64.Vec2
}

func newEnemy(renderer *sdl.Renderer, posX, posY float64) (Enemy, error) {
	var e Enemy
	var position mgl64.Vec2
	var err error

	e.sprite, err = engine.NewSprite(fmt.Sprintf(ASSETS_FOLDER_PATH, ENEMY_SPRITE_FILE), renderer)

	if err != nil {
		return Enemy{}, fmt.Errorf("error creating enemy sprite: %v", err)
	}

	position = mgl64.Vec2{posX, posY}

	e.position = position

	return e, nil
}

func (enemy Enemy) GetRenderTarget() engine.RenderTarget {
	return engine.RenderTarget{Sprite: enemy.sprite, Position: enemy.position, Height: ENEMY_SIZE, Width: ENEMY_SIZE}
}
