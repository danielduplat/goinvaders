package main

import (
	"GoInvaders/src/engine"

	"github.com/go-gl/mathgl/mgl64"
)

type gameMap struct {
	background      engine.Sprite
	backgroundColor engine.Color
	enemies         []Enemy
	player          Player
}

func newMap(bg engine.Sprite, bgColor engine.Color) gameMap {
	var gmap gameMap

	gmap.background = bg
	gmap.backgroundColor = bgColor

	return gmap
}

func (gmap gameMap) spawnPlayer(spawnPosition mgl64.Vec2, renderer engine.GameRenderer) {
	gmap.player.position = spawnPosition

	renderer.Draw(gmap.player, 1.0, 0)
}

func (gmap gameMap) spawnEnemy() {

}
