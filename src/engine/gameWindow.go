package engine

import (
	"github.com/go-gl/mathgl/mgl64"
	"github.com/veandco/go-sdl2/sdl"
)

type GameWindow struct {
	Instance      *sdl.Window
	Width, Height float64
}

func NewWindow(title string, height, width int32) GameWindow {
	var gameWindow GameWindow
	var err error

	gameWindow.Instance, err = sdl.CreateWindow(
		title,
		int32(sdl.WINDOWPOS_UNDEFINED), int32(sdl.WINDOWPOS_UNDEFINED),
		width, height,
		uint32(sdl.WINDOW_OPENGL))

	if err != nil {
		panic(err)
	}

	gameWindow.Height = float64(height)
	gameWindow.Width = float64(width)

	return gameWindow
}

func (gw *GameWindow) SetFullScreen() {
	maxW, maxH := gw.Instance.GetMaximumSize()
	W, H := gw.Instance.GetSize()

	if maxH != H && maxW != W {
		gw.Instance.SetFullscreen(sdl.WINDOW_FULLSCREEN_DESKTOP)
	}
}

func (gw *GameWindow) IsPositionOutOfBounds(center mgl64.Vec2, height, width uint32) bool {

	//Left side check
	if center.X()-float64(width)/2.0 < 0 {
		return true
	}

	//Right side check
	if center.X()+float64(width)/2.0 > gw.Width {
		return true
	}

	//Top side check
	if center.Y()-float64(height)/2.0 < 0 {
		return true
	}

	//Bottom side check
	if center.Y()+float64(height)/2.0 > gw.Height {
		return true
	}

	return false
}
