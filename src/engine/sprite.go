package engine

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

type Sprite struct {
	Texture *sdl.Texture
}

func NewSprite(spritePath string, renderer *sdl.Renderer) (Sprite, error) {
	var sprt Sprite

	img, err := sdl.LoadBMP(spritePath)

	if err != nil {
		return Sprite{}, fmt.Errorf("\nerror loading sprite img file: %v", err)
	}

	defer img.Free()

	sprt.Texture, err = renderer.CreateTextureFromSurface(img)

	if err != nil {
		return Sprite{}, fmt.Errorf("\nerror creating player texture: %v", err)
	}

	return sprt, nil
}
