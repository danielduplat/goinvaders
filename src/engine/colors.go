package engine

type Color struct {
	R, G, B, A uint8
}

type Colors struct {
	White Color
}

var COLORS = Colors{White: Color{R: 255, G: 255, B: 255, A: 255}}
