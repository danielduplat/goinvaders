package engine

import (
	"fmt"

	"github.com/veandco/go-sdl2/sdl"
)

type GameRenderer struct {
	Instance *sdl.Renderer
}

func InitRenderer(window *sdl.Window) (GameRenderer, error) {

	var renderer GameRenderer
	var err error

	renderer.Instance, err = sdl.CreateRenderer(window, -1, uint32(sdl.RENDERER_ACCELERATED))

	if err != nil {
		return GameRenderer{}, fmt.Errorf("error initalazing renderer: %v", err)
	}

	return renderer, nil
}

func (renderer *GameRenderer) SetDrawColor(color Color) {
	renderer.Instance.SetDrawColor(color.R, color.G, color.B, color.A)
}

func (renderer *GameRenderer) Draw(renderable IRenderTarget, scaleFactor, rotationAngle float64) {

	renderTarget := renderable.GetRenderTarget()

	scaledHeight := int32(scaleFactor * renderTarget.Height)
	scaledWidth := int32(scaleFactor * renderTarget.Width)

	X := int32(renderTarget.Position.X() - renderTarget.Width/2.0)
	Y := int32(renderTarget.Position.Y() - renderTarget.Height/2.0)

	renderer.Instance.CopyEx(renderTarget.Sprite.Texture,
		&sdl.Rect{X: 0, Y: 0, W: int32(renderTarget.Width), H: int32(renderTarget.Height)},
		&sdl.Rect{X: X, Y: Y, W: scaledWidth, H: scaledHeight},
		rotationAngle,
		&sdl.Point{X: int32(renderTarget.Width / 2), Y: int32(renderTarget.Height / 2)},
		sdl.FLIP_NONE)
}
