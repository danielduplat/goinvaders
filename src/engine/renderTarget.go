package engine

import "github.com/go-gl/mathgl/mgl64"

type IRenderTarget interface {
	GetRenderTarget() RenderTarget
}

type RenderTarget struct {
	Sprite   Sprite
	Position mgl64.Vec2
	Height   float64
	Width    float64
}
